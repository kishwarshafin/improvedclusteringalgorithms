/*
ID: kishwarshafin
PROG:
LANG: C++
*/
/*
Timus JI: 119454XP
*/
#include<iostream>
#include<vector>
#include<stack>
#include<string>
#include<queue>
#include<set>
#include<map>
#include<algorithm>
#include<sstream>
using namespace std;
#include<stdio.h>
#include<time.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#define MAX 100
#define INF 1<<23

#define I1(a) scanf("%d",&a)
#define I2(a,b) scanf("%d %d",&a,&b)
#define I3(a,b,c) scanf("%d %d %d",&a,&b,&c)
#define rep(i,s,e) for(i=s;i<e;i++)
#define repr(i,s,e) for(i=s;i>e;i--)


#define in(a) freopen(a,"r",stdin)
#define out(a) freopen(a,"w",stdout)
#define ll long long
ll BigMod(ll B,ll P,ll M){  ll R=1; while(P>0)  {if(P%2==1){R=(R*B)%M;}P/=2;B=(B*B)%M;} return R%M;}
#define ull unsigned long long
#define M 1000000007
#define SIZE 26000
int PRINT_IT;

struct node{
    int id;
    double data;
    bool operator< (const node& rhs)
    {
        if(data==rhs.data)
            return id<rhs.id;
        else
            return data<rhs.data;
    }
    bool operator> (const node& rhs)
    {
        if(data==rhs.data)
            return id>rhs.id;
        else
            return data>rhs.data;
    }
};

class heap{
    public:
    node A[SIZE];
    int MAP[SIZE];
    int heap_size;

    public:
        int init()
        {
            heap_size=1;
        }
        int parent(int i)
        {
            return i/2;
        }

        int left(int i)
        {
            return 2*i;
        }

        int right(int i)
        {
            return 2*i+1;
        }

        void max_heapify(int i)
        {
            int l=left(i);
            int r=right(i);

            int largest;
            if(l<=heap_size and A[l]>A[i])
            {
                largest=l;
            }
            else largest=i;

            if(r<=heap_size and A[r]>A[largest])
                largest=r;
            if(largest!=i)
            {
                MAP[A[i].id]=largest;
                MAP[A[largest].id]=i;
                swap(A[i],A[largest]);
                max_heapify(largest);
            }
        }

        void build_max_heap()
        {
            for(int i=heap_size/2;i>=1;i--)
                max_heapify(i);
        }

        node heap_max()
        {
            return A[1];
        }

        node extract_max()
        {
            node a;
            a.data=-1;
            a.id=-1;
            if(heap_size<1)
                return a;
            node max=A[1];

            MAP[A[1].id]=0;
            A[1]=A[heap_size];

            heap_size--;
            max_heapify(1);
            return max;
        }

        void Heap_Increase_Key(int i,node key)
        {
            if(key<A[i])return;
            A[i]=key;
            MAP[key.id]=i;

            while(i>1 and A[parent(i)]<A[i])
            {
                MAP[A[i].id]=parent(i);
                MAP[parent(i)]=i;
                swap(A[i],A[parent(i)]);
                i=parent(i);
            }
        }

        void Heap_Decrease_Key(int i,node key)
        {
            if(key>A[i])return;
            A[i]=key;
            MAP[key.id]=i;
            max_heapify(i);
        }
};

//MAIN CODE//
map<string,int>MP;
map<int,string>ULTA_MP;
vector<node>G[25000][5];
double node_weight[25000];
double DegreeQ[25000];
bool color[25000];
double EdgeW[25000];
int Connectivity[25000];
int inSet[25000];
set< pair<double,int> >S[5];
int n,m;
heap MN;

int get_bin(double val)
{
    if(val<=0.2)return 0;
    else if(val<=0.4)return 1;
    else if(val<=0.6)return 2;
    else if(val<=0.8)return 3;
    else return 4;
}
double get_range(int number)
{
    if(number==0)return 0.2;
    else if(number==1)return 0.4;
    else if(number==2)return 0.6;
    else if(number==3)return 0.8;
    else return 1.0;
}
void init()
{
    memset(node_weight,0,sizeof(node_weight));
}

void input()
{
    init();
    n=1;
    int w1,w2;double w;
    char x[1000];
    char y[1000];
    int m=0;
    while(scanf("%s %s %lf\n",x,y,&w)!=EOF)
    {
        m++;
        if(MP[x]==0)
        {
            ULTA_MP[n]=x;
            MP[x]=n++;
        }
        if(MP[y]==0)
        {
            ULTA_MP[n]=y;
            MP[y]=n++;
        }

        node e;
        e.id=MP[y];
        e.data=w;
        G[MP[x]][get_bin(w)].push_back(e);
        e.id=MP[x];
        G[MP[y]][get_bin(w)].push_back(e);
        node_weight[MP[x]]+=w;
        node_weight[MP[y]]+=w;
    }
    n--;
}

void init_cluster()
{
    memset(DegreeQ,0,sizeof(DegreeQ));
    for(int i=1;i<n;i++)
    {
        DegreeQ[i]=node_weight[i];
        for(int j=0;j<5;j++){
            for(int k=0;k<G[i][j].size();k++)
            {
                int v=G[i][j][k].id;
                DegreeQ[i]+=node_weight[v];
            }
        }
    }
    MN.init();
    for(int i=1;i<=n;i++)
    {
        MN.A[i].id=i;
        MN.A[i].data=DegreeQ[i];
        MN.MAP[i]=i;
    }
    MN.heap_size=n;
    MN.build_max_heap();
    memset(color,0,sizeof(color));
}


void perform_clustering(double Ts=0.5,double Ds=0.5,int min_cluster_size=0)
{

    heap SUB;
    init_cluster();
    while(MN.heap_size>0)
    {
        memset(Connectivity,0,sizeof(Connectivity));
        memset(EdgeW,0,sizeof(EdgeW));
        memset(inSet,-1,sizeof(inSet));

        for(int i=0;i<5;i++)
            S[i].clear();

        double Cluster_support=0;
        double Cluster_size=0;

        //FIRST SEED
        node First_seed=MN.heap_max();
        int f=First_seed.id;
        MN.extract_max();
        if(color[f]==1)continue;
        color[f]=1;
        Cluster_size++;


        if(PRINT_IT)
        printf("%s ",ULTA_MP[f].c_str());

        //SECOND SEED
        int exists=-1;
        node Second_seed;

        for(int i=4;i>=0;i--)
        {
            for(int j=0;j<G[f][i].size();j++)
            {
                int C=G[f][i][j].id;
                if(color[C]==1)continue;
                node key;
                key.id=C;
                key.data=MN.A[MN.MAP[C]].data-DegreeQ[f]-G[f][i][j].data;
                DegreeQ[C]-=G[f][i][j].data;
                MN.Heap_Decrease_Key(MN.MAP[C],key);
                if(exists==-1 && G[f][i][j].data>=Ds && G[f][i][j].data>=Ts)
                {
                    Second_seed=G[f][i][j];
                    exists=i;
                }
                else
                {
                    if(exists==i && node_weight[Second_seed.id]<=node_weight[C] && G[f][i][j].data>=Ds && G[f][i][j].data>=Ts)
                        Second_seed=G[f][i][j];
                }

                //UPDATE SUPPORT
                int u=C;
                double d=G[First_seed.id][i][j].data;

                double AvG=EdgeW[u]/(double)Connectivity[u];
                double support=EdgeW[u];

                if(inSet[u]==-1){
                    Connectivity[u]++;
                    EdgeW[u]+=d;
                    AvG=EdgeW[u]/(double)Connectivity[u];
                    support=EdgeW[u];

                    pair<double,int> P;
                    P=make_pair(support,u);
                    inSet[u]=get_bin(AvG);
                    S[inSet[u]].insert(P);
                }
                else{
                    pair<double,int> P;
                    P=make_pair(support,u);
                    S[inSet[u]].erase(S[inSet[u]].find(P));

                    Connectivity[u]++;
                    EdgeW[u]+=d;
                    AvG=EdgeW[u]/(double)Connectivity[u];
                    support=EdgeW[u];
                    P=make_pair(support,u);
                    inSet[u]=get_bin(AvG);
                    S[inSet[u]].insert(P);
                }
            }
        }


        if(exists==-1)
        {
            if(PRINT_IT)
            printf("\n");
            continue;
        }

        if(PRINT_IT)
        printf("%s ",ULTA_MP[Second_seed.id].c_str());

        color[Second_seed.id]=1;
        Cluster_size++;
        Cluster_support=Second_seed.data;

        //EXPANSION


        for(int i=4;i>=0;i--)
        {
            for(int j=0;j<G[Second_seed.id][i].size();j++)
            {
                if(color[G[Second_seed.id][i][j].id]==1)continue;


                int u=G[Second_seed.id][i][j].id;
                double d=G[Second_seed.id][i][j].data;

                node key;
                key.id=u;
                key.data=MN.A[MN.MAP[u]].data-DegreeQ[Second_seed.id]-d;
                DegreeQ[Second_seed.id]-=d;
                MN.Heap_Decrease_Key(MN.MAP[u],key);

                double AvG=EdgeW[u]/(double)Connectivity[u];
                double support=EdgeW[u];

                if(inSet[u]==-1){
                    Connectivity[u]++;
                    EdgeW[u]+=d;
                    AvG=EdgeW[u]/(double)Connectivity[u];
                    support=EdgeW[u];

                    pair<double,int> P;
                    P=make_pair(support,u);
                    inSet[u]=get_bin(AvG);
                    S[inSet[u]].insert(P);
                }
                else{
                    pair<double,int> P;
                    P=make_pair(support,u);
                    S[inSet[u]].erase(S[inSet[u]].find(P));

                    Connectivity[u]++;
                    EdgeW[u]+=d;
                    AvG=EdgeW[u]/(double)Connectivity[u];
                    support=EdgeW[u];
                    P=make_pair(support,u);
                    inSet[u]=get_bin(AvG);
                    S[inSet[u]].insert(P);
                }
            }
        }



        //ACTUAL EXPANSION

        while(1)
        {
            set< pair<double,int> >::iterator it;
            bool ache=0;
            int bin=-1;
            for(int i=4;i>=0;i--)
            {
                if(S[i].empty()==1)continue;

                ache=1;
                it=S[i].end();
                it--;
                bin=i;
                break;

            }
            if(ache==0)break;
            pair< double , int > P;
            P=*it;
            S[bin].erase(it);

            double new_size=(double)Cluster_size+1.0;
            double new_support=Cluster_support+P.first;
            double new_density=new_support/(new_size*(new_size-1)/2.0);
            double density=Cluster_support/((double)Cluster_size*((double)Cluster_size-1)/2.0);

            double rhs=Ts*Cluster_size*density;
            bool hobe=0;
            if(P.first>=rhs && new_density>=Ds)
            {
                if(PRINT_IT)
                printf("%s ",ULTA_MP[P.second].c_str());

                hobe=1;
                color[P.second]=1;
                Cluster_size++;
                Cluster_support=new_support;
            }
            else
                break;
            for(int i=4;i>=0;i--)
            {
                for(int j=0;j<G[P.second][i].size();j++)
                {
                    if(color[G[P.second][i][j].id]==1)continue;
                    int u=G[P.second][i][j].id;
                    double d=G[P.second][i][j].data;

                    node key;
                    key.id=u;
                    key.data=MN.A[MN.MAP[u]].data-DegreeQ[P.second]-d;
                    DegreeQ[P.second]-=d;
                    MN.Heap_Decrease_Key(MN.MAP[u],key);

                    double AvG=EdgeW[u]/(double)Connectivity[u];
                    double support=EdgeW[u];

                    if(inSet[u]==-1){
                        Connectivity[u]++;
                        EdgeW[u]+=d;
                        AvG=EdgeW[u]/(double)Connectivity[u];
                        support=EdgeW[u];

                        pair<double,int> P;
                        P=make_pair(support,u);
                        inSet[u]=get_bin(AvG);
                        S[inSet[u]].insert(P);
                    }
                    else{
                        pair<double,int> P;
                        P=make_pair(support,u);
                        S[inSet[u]].erase(S[inSet[u]].find(P));

                        Connectivity[u]++;
                        EdgeW[u]+=d;
                        AvG=EdgeW[u]/(double)Connectivity[u];
                        support=EdgeW[u];
                        P=make_pair(support,u);
                        inSet[u]=get_bin(AvG);
                        S[inSet[u]].insert(P);
                    }
                }
            }
        }

        if(PRINT_IT)
        printf("\n");
    }
}

int main()
{
    in("in.txt");
    out("out.txt");
    input();
    PRINT_IT=1;
    init_cluster();
    perform_clustering();
    return 0;
}

