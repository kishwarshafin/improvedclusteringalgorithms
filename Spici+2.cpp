#include<iostream>
#include<vector>
#include<stack>
#include<string>
#include<queue>
#include<map>
#include<algorithm>
#include<sstream>
using namespace std;
#include<stdio.h>
#include<time.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#define MAX 100
#define INF 1<<23

#define I1(a) scanf("%d",&a)
#define I2(a,b) scanf("%d %d",&a,&b)
#define I3(a,b,c) scanf("%d %d %d",&a,&b,&c)
#define rep(i,s,e) for(i=s;i<e;i++)
#define repr(i,s,e) for(i=s;i>e;i--)


#define in(a) freopen(a,"r",stdin)
#define out(a) freopen(a,"w",stdout)
#define ll long long
ll BigMod(ll B,ll P,ll M){  ll R=1; while(P>0)  {if(P%2==1){R=(R*B)%M;}P/=2;B=(B*B)%M;} return R%M;}
#define ull unsigned long long

template<class vt>
class fib_heap
{
	class fib_node
	{
	public:
		fib_node(const vt& v,int a)
			: parent(NULL)
			, child(NULL)
			, degree(0)
			, marked(false)
			, value(v)
			, id(a)
		{
		}

		fib_node *parent;
		fib_node *prev;
		fib_node *next;
		fib_node *child;
		size_t degree;
		bool marked;
		vt value;
		int id;
	};
public:
	class iterator
	{
	public:
		iterator()
			: p(NULL)
		{
		}

		iterator(fib_node *p_)
			: p(p_)
		{
		}

		vt& operator*() {return p->value;}
		vt* operator->() {return &p->value;}
		bool operator==(iterator& other){return p==other.p;}
		bool operator!=(iterator& other){return p!=other.p;}
	private:
		friend class fib_heap;
		fib_node *p;
	};

	fib_heap()
		: minRoot(NULL)
		, combineVec(16)
	{
	}

	~fib_heap()
	{
		clear();
	}

	iterator insert(const vt& v,int a)
	{
		fib_node *newNode = new fib_node(v,a);
		if (!minRoot) {
			minRoot = newNode->next = newNode->prev = newNode;
		} else {
			newNode->prev = minRoot;
			newNode->next = minRoot->next;
			minRoot->next->prev = newNode;
			minRoot->next = newNode;
			if (newNode->value < minRoot->value)
				minRoot = newNode;
		}
		return iterator(newNode);
	}

	bool empty()
	{
		return minRoot == NULL;
	}

	int min()
	{
		return minRoot->id;
	}
	double minVal(){
		return minRoot->value;
	}
	int minID(){
		return minRoot->id;
	}

	void deleteMax()
	{
		fib_node *child = minRoot->child;
		if (child) {
			fib_node *curchild = child;
			do {
				curchild->parent = NULL;
				curchild->marked = false;
				curchild = curchild->next;
			} while (child != curchild);
			child->prev->next = minRoot->next;
			minRoot->next->prev = child->prev;
			child->prev = minRoot;
			minRoot->next = child;
		}

		fib_node *curNode = minRoot->next;
		fib_node *nextNode;
		while (curNode != minRoot) {
			nextNode = curNode->next;
			size_t degree = curNode->degree;
			fib_node *target = combineVec[degree];
			while (target) {
				if (target->value < curNode->value) {
					fib_node *tmp = curNode;
					curNode = target;
					target = tmp;
				}
				target->next->prev = target->prev;
				target->prev->next = target->next;
				if (!curNode->child) {
					curNode->child = target->next = target->prev = target;
				} else {
					fib_node *child = curNode->child;
					target->prev = child;
					target->next = child->next;
					child->next->prev = target;
					child->next = target;
				}
				target->parent = curNode;
				combineVec[degree] = NULL;
				degree = ++curNode->degree;
				if (degree > combineVec.size())
					combineVec.resize(degree);
				target = combineVec[degree];
			}
			combineVec[degree] = curNode;
			curNode = nextNode;
		}

        curNode = minRoot->next;
		fib_node *curMin = NULL;
		while (curNode != minRoot) {
			combineVec[curNode->degree] = NULL;
			if (!curMin || curNode->value < curMin->value)
				curMin = curNode;
			curNode = curNode->next;
		}
		minRoot->next->prev = minRoot->prev;
		minRoot->prev->next = minRoot->next;
		delete minRoot;
		minRoot = curMin;
	}

	void increase(iterator it)
	{
		fib_node *node = it.p;
		fib_node *parent = node->parent;
		if (parent) {
			if (!(node->value < parent->value))
				return;
			cut(node);
		}
		if (node->value < minRoot->value)
			minRoot = node;
	}

	void erase(iterator it)
	{
		fib_node *node = it.p;
		if (node->parent) {
			cut(node);
		} else if (node == minRoot) {
			deleteMax();
			return;
		}
		fib_node *child = node->child;
		if (child) {
			fib_node *curchild = child;
			do {
				curchild->parent = NULL;
				curchild->marked = false;
				curchild = curchild->next;
			} while (child != curchild);
			child->prev->next = node->next;
			node->next->prev = child->prev;
			child->prev = node->prev;
			node->prev->next = child;
		} else {
			node->next->prev = node->prev;
			node->prev->next = node->next;
		}
		delete node;
	}

	void clear()
	{
		clearDescendants(minRoot);
		minRoot = NULL;
	}

private:
	void cut(fib_node *node)
	{
		do {
			fib_node *parent = node->parent;
			if (--parent->degree == 0) {
				parent->child = NULL;
			} else {
				node->next->prev = node->prev;
				node->prev->next = node->next;
				parent->child = node->next;
			}
			node->parent = NULL;
			node->marked = false;
			node->next = minRoot->next;
			node->prev = minRoot;
			minRoot->next->prev = node;
			minRoot->next = node;
			node = parent;
		} while (node->marked);
		if (node->parent)
			node->marked = true;
	}

	void clearDescendants(fib_node *child)
	{
		if (!child)
			return;
		fib_node *curchild = child;
		do {
			clearDescendants(curchild->child);
			fib_node *tmp = curchild;
			curchild = curchild->next;
			delete tmp;
		} while (curchild != child);
	}
private:
	fib_node *minRoot;
	std::vector<fib_node *> combineVec;
};

struct edge{
    int x;
    int y;
    double w;
};
class network{
public:
    vector<int> *V;
    vector<double> *E;
    map<string,int>MP;
    map<int,string>UMP;
    int in;
    int v;
    int e;
    double *out_degree;
    double *neighbour_out_degree;
    void IniTnetwork(int n){
        V=new vector<int>[n];
        E=new vector<double>[n];
        out_degree=new double[n];
        neighbour_out_degree=new double[n];
        for(int i=0;i<n;i++)out_degree[i]=0.0,neighbour_out_degree[i]=0.0;
    }
    void take_input()
    {
        char A[1000],B[1000];
        double w;
        vector<edge>EL;
        in=1;
        while(scanf("%s %s %lf",A,B,&w)==3)
        {
            edge E;
            E.x=StringToInt(A);
            E.y=StringToInt(B);
            E.w=w;
            EL.push_back(E);
        }
        IniTnetwork(in);
        v=in;
        e=EL.size();

        for(int i=0;i<EL.size();i++)
            insert_edge(EL[i].x,EL[i].y,EL[i].w);
        for(int i=0;i<EL.size();i++)
        {
            neighbour_out_degree[EL[i].x]+=out_degree[EL[i].y];
            neighbour_out_degree[EL[i].y]+=out_degree[EL[i].x];
        }
    }

    void insert_edge(int x,int y,double w){
        V[x].push_back(y);
        V[y].push_back(x);
        E[x].push_back(w);
        E[y].push_back(w);
        out_degree[x]+=w;
        out_degree[y]+=w;
        neighbour_out_degree[x]+=w;
        neighbour_out_degree[y]+=w;
    }

    int StringToInt(string a){
        if(MP.find(a)!=MP.end())return MP[a];
        else return UMP[in]=a,MP[a]=in++;
    }
    string IntToString(int a){
        return UMP[a];
    }

    void print() //NEVER CALLED JUST FOR DEBUGGING PURPOSE
    {
        for(int i=1;i<v;i++)
        {
            cout<<i<<": ";
            for(int j=0;j<V[i].size();j++)
            {
                cout<<V[i][j]<<" ";
            }
            cout<<endl;
        }
    }
};
class FIBO_HEAP{
    fib_heap<double>F;
    vector<fib_heap<double>::iterator>V;
    map<int,int>MP;
    int ite;
public:
        FIBO_HEAP(int n)
        {
            MP.clear();
            ite=0;
        }
        double ExtractMax()
        {
            double val=F.min();
            F.deleteMax();
            return val;
        }
         void Insert(int n,double val){
            fib_heap<double>::iterator it;
            it=F.insert(val*(-1.0),n);
            V.push_back(it);
            MP[n]=ite++;
        }

        double get_val(int n){
            return *V[MP[n]];
        }
        void IncreaseKey(int n,double val){
            *V[MP[n]]=get_val(n)+val*(-1.0);
            F.increase(V[MP[n]]);
        }
        void InsertOrIncrease(int n,double val){
            if(MP.find(n)!=MP.end()){
                IncreaseKey(n,val);
            }
            else{
                Insert(n,val);
            }
        }
        bool empty(){
            return F.empty();
        }
        int minID(){
            return F.minID();
        }
        int minVal(){
            return F.minVal()*(-1);
        }
        ~FIBO_HEAP(){
            F.clear();
        }
};

class FIBO_HEAP2{
    fib_heap<double>F;
    vector<fib_heap<double>::iterator>V;
    map<int,int>MP;
    int ite;
public:
        FIBO_HEAP2(int n)
        {
            MP.clear();
            ite=0;
        }
        double ExtractMax()
        {
            double val=F.min();
            F.deleteMax();
            return val;
        }
         void Insert(int n,double val){
            fib_heap<double>::iterator it;
            it=F.insert(val*(-1.0),n);
            V.push_back(it);
            MP[n]=ite++;
        }

        double get_val(int n){
            return *V[MP[n]];
        }
        void DecreaseKey(int n,double val){
            *V[MP[n]]=get_val(n)+val;
            F.increase(V[MP[n]]);
        }
        void InsertOrDecrease(int n,double val){
            if(MP.find(n)!=MP.end()){
                DecreaseKey(n,val);
            }
            else{
                Insert(n,val);
            }
        }
        bool empty(){
            return F.empty();
        }
        int minID(){
            return F.minID();
        }
        double minVal(){
            return F.minVal()*(-1);
        }
        void Delete(int n){
            F.erase(V[MP[n]]);
        }
        ~FIBO_HEAP2(){
            F.clear();
        }
};
bool clustered[10000000];
class Clustering_algorithm{
    int V;
    int E;
    double Ts,Td;
    network N;
    public:
        void inp()
        {
            N.take_input();
            V=N.v;
            E=N.e;
        }
        void set_Ts(double a){
            Ts=a;
        }
        void set_Td(double a){
            Td=a;
        }
        int roundToint(double a){
            if(a<=0.0)return 0;
            return (int)(a+0.5);
        }
        int which_stack(double a){
            if(a>0.8)return 1;
            if(a>=0.6)return 2;
            if(a>=0.4)return 3;
            if(a>=0.2)return 4;
            else return 5;
        }

        double calculate_density(double Weighted_degree, double Cluster_Size){
            if(Cluster_Size==0)return 0;
            return (Weighted_degree)/((Cluster_Size*(Cluster_Size-1))/2.0);
        }
        void clustering_algorithm(){ //OUR MAIN CLUSTERING ALGORITHM
            memset(clustered,0,sizeof(clustered));
            FIBO_HEAP2 D(V+1);

            for(int i=1;i<V;i++)
                D.InsertOrDecrease(i,N.neighbour_out_degree[i]);

            int clsd=0;
            while(1){
                double Cluster_Size=0;
                double Weighted_degree=0;
                FIBO_HEAP F(V+1);

                //SEED SELECTION
                if(D.empty()==true)break;
//                printf("%d %lf\n",D.minID(),D.minVal());
//                cout<<D.minID()<<" "<<D.minVal()<<endl;
                int u=D.ExtractMax(),v=-1,mx=0;

                if(clustered[u])continue;


                double mx2=0;
                for(int i=0;i<N.V[u].size();i++){
                    int pv=N.V[u][i];
                    if(clustered[pv])continue;

                    int st=which_stack(N.E[u][i]);
                    if(clustered[pv]==0 && v==-1)v=pv,mx=st,mx2=N.neighbour_out_degree[pv];
                    else if(clustered[pv]==0 && mx>=st && mx2<N.neighbour_out_degree[pv])
                        v=pv,mx=st,mx2=N.neighbour_out_degree[pv];

                    D.InsertOrDecrease(pv,N.out_degree[u]+N.E[u][i]);
                    F.InsertOrIncrease(pv,N.E[u][i]);

                    N.neighbour_out_degree[pv]-=N.out_degree[u];
                    N.out_degree[pv]-=N.E[u][i];
                    if(v==pv){
                        Weighted_degree=N.E[u][i];
                    }
                }


                if(v==-1){
                    clustered[u]=1;
                    printf("%s\n",N.IntToString(u).c_str());
                    clsd++;
                    continue;
                }
                Cluster_Size=2;
                clustered[u]=1,clustered[v]=1;
                D.Delete(v);
                clsd+=2;

                //update the support
                for(int i=0;i<N.V[v].size();i++){
                    int pv=N.V[v][i];
                    if(clustered[pv])continue;

                    D.InsertOrDecrease(pv,N.out_degree[v]+N.E[v][i]);
                    F.InsertOrIncrease(pv,N.E[v][i]);

                    N.neighbour_out_degree[pv]-=N.out_degree[v];
                    N.out_degree[pv]-=N.E[v][i];
                }

                printf("%s %s ",N.IntToString(u).c_str(),N.IntToString(v).c_str());

                //---------------------SEED SELECTION ENDS HERE-----------------------------//
                while(F.empty()!=true){

                    int support=F.minVal();
                    int candidate=F.ExtractMax();
                    if(clustered[candidate])continue;

                    double current_density=calculate_density(Weighted_degree,Cluster_Size);
                    double union_density=calculate_density(Weighted_degree+support,Cluster_Size+1);
                    double mxval=current_density*Cluster_Size*Ts;
                    if(union_density>=Td && support>=mxval){
                        printf("%s ",N.IntToString(candidate).c_str());

                        Weighted_degree+=support;
                        Cluster_Size+=1.0;
                        clustered[candidate]=1;
                        for(int i=0;i<N.V[candidate].size();i++){
                            int pv=N.V[candidate][i];
                            if(clustered[pv])continue;

                            D.InsertOrDecrease(pv,N.out_degree[candidate]+N.E[candidate][i]);
                            F.InsertOrIncrease(pv,N.E[candidate][i]);

                            N.neighbour_out_degree[pv]-=N.out_degree[candidate];
                            N.out_degree[pv]-=N.E[candidate][i];
                        }
                        D.Delete(candidate);
                        clsd++;
                    }
                }
                printf("\n");
            }
        }
};

int main()
{
    in("in.txt");
    out("out.txt");
    Clustering_algorithm a;
    a.inp();
    a.set_Ts(0.5);
    a.set_Td(0.5);
    a.clustering_algorithm();
    return 0;
}
