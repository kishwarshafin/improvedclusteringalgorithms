1) To generate clusters using improved SPICi algorithm first we need have a input network in a file.
Suppose a graph G={A,B,C,D} where A,B,C,D are four nodes. They have various edge weights then the input file should be:

NODE1 NODE2 EDGE_WEIGHT_BETWEEN_NODE1_AND_NODE2

For example:

A B 1.0

B D 0.5

Or for a test you can download the benchmark networks from:

http://compbio.cs.princeton.edu/spici/files/benchmark_network.tar.gz

Downloads>Test Network
The files:

Biogrid 4932: Biogrid Yeast

STRING 4932: STRING Yeast

Biogrid 9606: Briogrid Human

Biogrid 9606: String Human

[To change Ts and Td you will find a function:
void perform_clustering(double Ts=0.5,double Ds=0.5,int min_cluster_size=0)
you can changes the values here.]

 
2) Now in the main function of the each code we have to give the directory of the network we want to cluster.

And the output file will be generated according to the file name we give in out() function in the main function. 

3) The output of each input can be described like this:

Each line of the output file corresponds to a single cluster.

Please use a editor like notepad++ to have clear understanding about the output clusters.